initialize({ token: 'your-token-here', apiKey: 'your-api-key-here' })

function initialize({ token, apiKey }) {
  fetch('https://prod.api.platform.rentmount.com/components-sdk/client-secret?token=' + token, {
    headers: {
      'api_key': apiKey,
    }
  })
    .then(response => {
      return response.json()
    })
    .then(data => {
      return data['clientSecret']
    })
    .catch(error => {
      console.log(error)
    })
}
