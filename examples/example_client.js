const mountSDK = new MountSDK({ appId: 'your-app-id' });

const options = {
  clientSecret: 'your-client-secret',
};

const listingsWidget = mountSDK.create({ componentName: 'listings-widget', options: options });

listingsWidget.render({ componentId: 'widget-container' });
