# Mount Components SDK

## Introduction

Mount Components are modular web elements that expose Mount's core listing functionality. The Mount Components SDK is a simple Javascript module that allows developers to embed Mount Components anywhere. Embedding Mount Components allows for a highly-customizable, cobranded integration with Mount.

## Getting started

### Authentication

#### API Key

Mount Components SDK users must have an API key to successfully integrate with Mount. To obtain an API key and an App Id, please contact Mount at travis@rentmount.com.

#### Client Secret

##### Obtaining a Marketplace Token

The Mount Components SDK requires a client secret to authenticate requests. To obtain a client secret, a marketplace token must be provided for the marketplace you would like to get a client secret for. Soon, Mount users will have the ability to retrieve the token for their marketplace on their Mount Dashboard. For now, to obtain a marketplace token, instruct your users to contact Mount at travis@rentmount.com, and ask for their Marketplace Token. Make sure each user includes their name and the name of their marketplace in the email. Once the user has received their marketplace token, they can provide it to you to generate a client secret.

##### Generating a Client Secret

Mount Components are created using a client secret. This allows components rendered in your frontend to be securely connected directly to Mount. To generate a client secret, simply make a GET request on the endpoint specified below. A valid API key must be included in the "api_key" header of the request. This request must be made server-side in order to maintain API key integrity. A client secrets are generated upon sending the request. After the component is rendered, the client secret is no longer valid. A new client secret must be generated for each component render.

```
GET https://prod.api.platform.rentmount.com/components-sdk/client-secret?token={marketplace-token}
```

If you would like to test the SDK and haven't yet created a marketplace, you can use the following sandbox marketplace token:

```
452a2e84-8467-4692-bb76-a02c37a00f50
```

### Installation

Include the following tag in the head of the html page that will embed Mount Components.

```
<script src="https://embeds.platform.rentmount.com/mount_components_sdk.js"></script>
```

Additionally, include your own script tag that will contain your code for creating and rendering components, or include the javascript client code in your html. See the [example usage](#example-usage) section for more information.

### Usage


#### Instantiating the SDK

Once you have received your App Id and Client Secret, the Mount Components SDK can be initialized on your client.

```
const mountSDK = new MountSDK({ appId: 'your-appId-here' });
```

#### Creating a component

Creating components is done using the SDK's "create" method. The create method takes in a component type and a set of options. To see our collection of supported component types, [click here](#component-types).

```
const options = {
  clientSecret: 'your-client-secret-here',
};

const listingsWidget = mountSDK.create({ componentName: 'listings-widget', options: options });
```

#### Rendering a component

Rendering a component attaches the component to a specified element in the html. Rendering is performed using the SDK's "render" method.

```
listingsWidget.render({ componentId: 'widget-container' });
```

The componentId is the ID of the HTML element that the Mount Component will attach to.

## Component Types

The Mount Components SDK supports the following component types:

- Listings Widget ('listings-widget')

  The Listings Widget renders a view of all the listings contained in the Mount Mini-Marketplace specified by the client secret. The Listings Widget is a responsive component that will adjust its layout based on the width of the container it is attached to.

  ![Alt text](/images/listings-widget.png?raw=true "Listings Widget")


## Example Usage

Example templates for SDK installation and html templating can be found in the "examples" folder of this repository, as well as below.

### HTML Template

This file can also be downloaded here: [example_html_template.html](/examples/example_html_template.html)

```html
<html>
  <head>
      <script src="https://embeds.platform.rentmount.com/mount_components_sdk.js"></script>
  </head>
  <body>
    <div id="widget-container">

    </div>
    <script src="example_client.js"></script>
  </body>
</html>
```

or

[alternate_example_html_template.html](/examples/alternate_example_html_template.html)

```html
<html>
  <head>
    <script src="https://embeds.platform.rentmount.com/mount_components_sdk.js"></script>
  </head>
  <body>
    <div id="widget-container">

    </div>

    <script>
      const mountSDK = new MountSDK({ appId: 'your-app-id' });

      const options = {
        clientSecret: 'your-client-secret',
      };

      const listingsWidget = mountSDK.create({ componentName: 'listings-widget', options: options });

      listingsWidget.render({ componentId: 'widget-container' });
    </script>
  </body>
</html>
```

### Server-side code

This file can also be downloaded here: [example_server.js](/examples/example_server.js)

```javascript
initialize({ token: 'your-token-here', apiKey: 'your-api-key-here' })

function initialize({ token, apiKey }) {
  fetch('https://prod.api.platform.rentmount.com/components-sdk/client-secret?token=' + token, {
    headers: {
      'api_key': apiKey,
    }
  })
    .then(response => {
      return response.json()
    })
    .then(data => {
      return data['clientSecret']
    })
    .catch(error => {
      console.log(error)
    })
}
```

### Client-side code

This file can also be downloaded here: [example_client.js](/examples/example_client.js)

```javascript
  const mountSDK = new MountSDK({ appId: 'your-appId-here' });
  const options = {
    clientSecret: 'your-client-secret-here',
  };
  const listingsWidget = mountSDK.create({ componentName: 'listings-widget', options: options });
  listingsWidget.render({ componentId: 'widget-container' });
```

## Support

Please reach out to travis@rentmount.com with any questions or issues.
